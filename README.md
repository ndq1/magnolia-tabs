# Magnolia tabs

Magnolia tabs is a light module for Magnolia CMS which provides you a Tabs component and tabs macro to segregate your content.

## Installation
With Magnolia CLI just run: `mgnl install magnolia-tabs`.

Alternatively git clone it from `https://bitbucket.org/ndq1/magnolia-tabs` and copy source files that you need into your Magnolia's modules directory.

## Availability

To be able to use tabs as a component in your template add tabs as available component in any area you want by using Magnolia CLI:

`mgnl add-availability  magnolia-tabs:components/tabs/tabs  <path-to-page[@area]> [options]`

or by adding properties in definition manually:

```
...
areas:
  some-area:
    availableComponents:
      tabs:
        id: magnolia-tabs:components/tabs/tabs
...
```

You also will need some components to add in your tab.
Edit `availableComponents` property in the same way in `templates/components/tabs/subcomponents/tab.yaml` file

## CSS and JS
You can find source files in `webresources/src` folder. Import and modify according to your needs.

## Usage
After successful installation you can use it both ways: as a component or as a macro by including it to your template.

'Tabs' will be available as a component when creating a new component in selected area. 

And you can use macro like this:
```
[#include './macros/tabs.ftl']

[@tabs; scope]
    [@tab name='tab1' displayName='Tab one' active=true scope=scope]
        Tab1: Lorem ipsum dolor sit amet
    [/@tab]
    
    [@tab name='tab2' displayName='Tab two' scope=scope]
        Tab2: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    [/@tab]
    
    [@tab name='tab3' displayName='Tab three' scope=scope]
        Tab3: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa dolore eveniet exercitationem iste molestiae perspiciatis quibusdam recusandae saepe sapiente veniam.
    [/@tab]
[/@tabs]
```
or combining with other directives like include or loop:
```
[#include '../../macros/tabs.ftl']
[#assign data = [
    {'name': 'tab1', 'displayName': 'Tab one', 'active': true, 'content': 'Tab1: Lorem ipsum dolor sit amet'},
    {'name': 'tab2', 'displayName': 'Tab two', 'active': false, 'content': 'Tab2: Lorem ipsum dolor sit amet, consectetur adipisicing elit.'},
    {'name': 'tab3', 'displayName': 'Tab three', 'active': false, 'content': 'Tab3: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa dolore eveniet exercitationem iste molestiae perspiciatis quibusdam recusandae saepe sapiente veniam.'}
]]

[@tabs; scope]
    [#list data as el]
        [@tab?with_args(el) scope=scope]
          ${el.content!}
        [/@tab]
    [/#list]
[/@tabs]
```

Keep in mind, that all `scope` occurrences are required to work properly

You can pass to each tab `name`, `displayName` and `active` as a parameters and any HTML you want between opening and closing tags

Both examples will output whole HTML with triggers and content segregated in their own containers respectively:
```html
<section class="tabs">
    <nav aria-label="Tabs navigation">
        <ul class="triggers-container" role="tablist">
            <li class="trigger-item" role="presentation">
                <a href="#tab-content-tab1"
                   id="tab-trigger-tab1"
                   class="tab-trigger active"
                   aria-selected="true"
                   aria-controls="tab-content-tab1"
                   role="tab">
                    Tab one
                </a>
            </li>
            <li class="trigger-item" role="presentation">
                <a href="#tab-content-tab2"
                   id="tab-trigger-tab2"
                   class="tab-trigger active"
                   aria-selected="false"
                   aria-controls="tab-content-tab2"
                   role="tab">
                    Tab two
                </a>
            </li>
        </ul>
    </nav>

    <section class="content-container" aria-live="polite">
        <section id="tab-content-tab1"
                 class="tab-content active"
                 aria-hidden="false"
                 aria-labelledby="tab-trigger-tab1"
                 role="tabpanel">
            Tab1: Lorem ipsum dolor sit amet
        </section>
        <section id="tab-content-tab2"
                 class="tab-content"
                 aria-hidden="true"
                 aria-labelledby="tab-trigger-tab2"
                 role="tabpanel">
            Tab2: Lorem ipsum dolor sit amet
        </section>
    </section>
</section>
```
