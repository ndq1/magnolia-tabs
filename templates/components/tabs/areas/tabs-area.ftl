[#include "../../../macros/tabs.ftl"]

[#if components?has_content]
  [@tabs; scope, editable]
    [#list components as component]
      [@cms.component content=component editable=editable contextAttributes={"scope":scope}/]
    [/#list]
  [/@tabs]
[/#if]
