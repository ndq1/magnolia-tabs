[#include "../../../macros/tabs.ftl"]

[#assign decoded = cmsfn.decode(content)]
[#assign name = content.tabName]
[#assign displayName = content.tabDisplayName]
[#assign active = content.active]
[#assign isRichText = content.type?has_content && content.type == 'RichText']

[@tab name=name displayName=displayName active=active scope=ctx.scope]
    [#if isRichText]
        ${decoded.typeRichText!}
    [#else]
        [@cms.area name="tab-content-area" /]
    [/#if]
[/@tab]
