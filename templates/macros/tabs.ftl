[#macro tabs name='' displayName='']
  <section class="tabs">
    <nav aria-label="Tabs navigation">
      <ul class="triggers-container" role="tablist">
        [#nested 'trigger' false]
      </ul>
    </nav>

    <section class="content-container" aria-live="polite">
      [#nested 'content' true]
    </section>
  </section>
[/#macro]

[#macro tab name='' displayName='' active=false scope='' rest...]
  [#if scope == 'trigger']
    [#local isActive = active?then('active', '')]

    <li class="trigger-item" role="presentation">
      <a href="#tab-content-${name?url}"
         id="tab-trigger-${name?url}"
         class="tab-trigger ${isActive!}"
         aria-selected="${active?c}"
         aria-controls="tab-content-${name?url}"
         role="tab">
        ${displayName!}
      </a>
    </li>
  [/#if]

  [#if scope == 'content']
    [#local isEditOrActive = cmsfn.editMode || active]
    [#local isActive = isEditOrActive?then('active', '')]


    <section id="tab-content-${name?url}"
             class="tab-content ${isActive!}"
             aria-hidden="${(!isEditOrActive)?c}"
             aria-labelledby="tab-trigger-${name?url}"
             role="tabpanel">

      [#if cmsfn.editMode]
        <div class="edit-mode-info">${displayName!} content ${active?then('(Active by default)', '')!}</div>
      [/#if]

      [#nested]
    </section>
  [/#if]
[/#macro]
