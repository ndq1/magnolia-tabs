export default class Tabs {
  constructor (target) {
    this.tabSection = target;
    this.tabTriggers = target.querySelectorAll('.triggers-container .tab-trigger');
    this.tabContents = target.querySelectorAll('.content-container .tab-content');
    this.defaultTrigger = [...this.tabTriggers].filter((el) => el.classList.contains('active'))[0];
    this.defaultTabContent = [...this.tabContents].filter((el) => el.classList.contains('active'))[0];
  }

  init () {
    if (!this.tabSection) {
      return;
    }

    this.bindListeners();
    this.revealTabByUrlHash();
  }

  revealTabByUrlHash () {
    window.dispatchEvent(new HashChangeEvent('hashchange'));
  }

  hideAllTabs () {
    this.tabContents.forEach(el => {
      el.classList.remove('active');
      el.setAttribute('aria-hidden', true);
    });
    this.tabTriggers.forEach(el => {
      el.classList.remove('active');
      el.setAttribute('aria-selected', false);
    });
  }

  openTab (contentId) {
    const shownEvent = new Event('shown');
    const targetTrigger = [...this.tabTriggers].filter((el) => el.hash === `#${contentId}`)[0] || this.defaultTrigger;
    const targetContent = [...this.tabContents].filter((el) => el.id === contentId)[0] || this.defaultTabContent;

    targetTrigger.classList.add('active');
    targetTrigger.setAttribute('aria-selected', true);

    targetContent.classList.add('active');
    targetContent.dispatchEvent(shownEvent);
    targetContent.setAttribute('aria-hidden', false);
  }

  bindListeners () {
    this.onClick();
    this.onHashChange();
  }

  onClick () {
    this.tabTriggers.forEach(el => {
      el.addEventListener('click', (e) => {
        e.preventDefault();

        window.history.replaceState({}, '', e.target.hash);
        document.dispatchEvent(new HashChangeEvent('hashchange'));
      });
    });
  }

  onHashChange () {
    document.addEventListener('hashchange', () => {
      const contentId = window.location.hash.slice(1);

      if (contentId) {
        this.hideAllTabs();
        this.openTab(contentId);
      }
    });
  }
}
